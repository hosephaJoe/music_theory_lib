#
# music_theory_lib
#
# Set of tools for calculating elements of music theory
#
# constants.py
#
# set of constant data used in describing theory
#
# Author : Joe Lamont-Fisher
#

import collections

# natural notes are the 7 notes we define in an octave. A-G
# cents divide up the octave - 1200 in an octave. Notes are irregularly spread
# but these values are consistent regardless of scale. Assuming basic theory
# as taught in the UK. Not yet dealing with other languages/regions
natural_cent_intervals = collections.OrderedDict()
natural_cent_intervals["A"] = 200
natural_cent_intervals["B"] = 200
natural_cent_intervals["C"] = 100
natural_cent_intervals["D"] = 200
natural_cent_intervals["E"] = 200
natural_cent_intervals["F"] = 100
natural_cent_intervals["G"] = 200


#    Each natural note can be modified with an accidental. This can flatten or sharp
#    the note one or 2 steps. Note that this does mean that in some cases you get overlap
#    I think this is called enharmonic equivalence? e.g. natural C is the same as Bsharp
#    D double sharp is same as E
accidental_intervals = collections.OrderedDict()
accidental_intervals["bb"] = -200
accidental_intervals["b"] = -100
accidental_intervals["n"] = 0
accidental_intervals["#"] = 100
accidental_intervals["##"] = 200


# The steps in a scale are either whole or half steps
scale_steps = {
    "W": 200,
    "H": 100
}

# list of scale formula
major_scale_formula = "WWHWWWH"

# map of chord formula to their readable names
# chords formula are made up of the index of the desired tone in the chord's scale
# and an accidental modification
chord_formulas = {
    "major": ["1", "3", "5"],
    "minor": ["1", "3b", "5"]
}
