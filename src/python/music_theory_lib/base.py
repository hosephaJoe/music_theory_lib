#
# music_theory_lib
#
# Set of tools for calculating elements of music theory
#
# base.py
#
# core logic for detemining music theory calculations
#
# Author : Joe Lamont-Fisher
#

import re

# The only python 3 required change was make the internal package reference
# python 3 friendly
from . import constants


class ScaleException(Exception):
    pass


class Note(object):
    """
    Represents a note as its natural tone plus any accidental modification.

    provides methods for translating a string in to the parts and the reverse.
    Used to find correct positions in scales.

    """

    note_parts = ["natural", "accidental"]

    def __init__(self, natural, accidental=None):
        """
        Create instance of the place, raise ValueError for invalid natural or accidental

        Args:
            natural (str): Natural tone string
            accidental (str): Accidental tone string
        """

        super(Note, self).__init__()

        if natural not in constants.natural_cent_intervals.keys():
            raise ValueError(f"natural {natural} not in valid natural note list")

        self._natural = natural

        self._accidental = ''
        if accidental:
            if accidental not in constants.accidental_intervals.keys():
                raise ValueError(f"natural {accidental} not in valid accidental list")

            self._accidental = accidental

    def __eq__(self, other):
        if self.natural is other.natural and self.accidental is other.accidental:
            return True

    @property
    def accidental(self):
        return self._accidental

    @property
    def natural(self):
        return self._natural

    @staticmethod
    def string_to_note_dict(note_string):
        """
        Convert string to a dictionary containing natural and accidental

        Args:
            note_string (str): The string representing the note. e.g. "E" or "Ab"

        Returns:
            dict: contains {note part name (str) : note part value (str)} where the key is the name of the note part
                and the value th
        """

        # get lists of all possible values for each part and join them for regex expressions
        note_str = "".join(list(constants.natural_cent_intervals.keys()))
        accidental_str = "|".join(list(constants.accidental_intervals.keys()))

        # non-greedy lookup for accidental allows it to be optional
        regex = f"(?P<{Note.note_parts[0]}>[{note_str}])(?P<{Note.note_parts[1]}>({accidental_str})?$)"
        m = re.match(regex, note_string)

        if not m:
            raise ValueError(f"Invalid note string passed: {note_string}")

        return m.groupdict() if m else {}

    @classmethod
    def from_str(cls, note_string):
        """
        Instantiate a Note class from a string representaion

        Args:
            note_string (str): The string representing the note. e.g. "E" or "Ab"
        """

        note_dict = Note.string_to_note_dict(note_string)
        return Note(note_dict["natural"], note_dict["accidental"])

    def __str__(self):
        return "".join([self._natural, self._accidental])

    def to_dict(self):
        """
        return a dictionary representing the note parts
        """
        return {"natural": self._natural, "incidental": self._accidental}


class Scale(object):
    """
    Represents a scale based on the passed root and formula. Uses that information to
    calculate scale tones, chord tones. These calculations are based on a set of interval
    between each Note.

    A scale is typically constructed of 7 steps and features 1 of each natural note with some
    accidental. Hmmm describing this is harder than writing the code. The steps are
    either (W)hole or (H)alf steps.

    A chord is comprised of some notes from the scale according to some formula.

    """

    def __init__(self, scale_root, scale_formula):
        """
        Args:
            scale_root (Note): note representing the root (first note) of the scale
            scale_formula (str): string representing the intervals between each scale tone.
                e.g. WWHWWWH
        """
        super(Scale, self).__init__()
        self._scale_root = scale_root
        self._scale_formula = scale_formula
        self._scale_tones = self._get_scale_tones()

    def __str__(self):
        return " ".join(self._scale_tones)

    @classmethod
    def major_scale_from_root_str(cls, root_note_string):
        """
        Helper to instantiate a scale object based on a note string

        Args:
            root_note_string (str): The string representing the note. e.g. "E" or "Ab"

        Returns:
            Scale : Instance of this object
        """
        note = Note.from_str(root_note_string)
        return cls(note, constants.major_scale_formula)

    def get_chord(self, name):
        """
        Function to return the notes of a chord based on this scale instance

        #TODO: Should probably take the formula and provide a helper

        Args:
            name (str) : Name of a chord formula

        Returns:
            list : list of tones for the scale

        """
        chord_formula = constants.chord_formulas.get(name)
        chord_tones = []
        if not chord_formula:
            raise ScaleException(f"No chord defined for name {name}")

        # build list of notes based on the indexes from the formula
        for i in range(1, len(self._scale_tones) + 1):
            if str(i) in chord_formula:
                chord_tones.append(self._scale_tones[i - 1])

        return chord_tones

    def _get_scale_tones(self):
        """
        Internal method to find the tones in the scale

        Returns:
            list: list of the tones
        """
        naturals = list(constants.natural_cent_intervals.keys())
        scale_tones = [str(self._scale_root)]
        natural_index = naturals.index(self._scale_root.natural)

        # get our starting cent modifier
        next_interval_modifier = constants.accidental_intervals.get(
            self._scale_root.accidental, 0
        )

        for scale_interval in self._scale_formula:
            # move to the next natural in the list
            natural_index = (
                natural_index + 1 if natural_index < len(naturals) - 1 else 0
            )
            natural_tone = naturals[natural_index]

            # get the cent interval we want to step and the step top the next natural
            cent_step = constants.scale_steps[scale_interval]
            natural_step = constants.natural_cent_intervals[natural_tone] - next_interval_modifier

            # if next step is the same as the natural step then we move to the natural
            if cent_step == natural_step:
                scale_tones.append(natural_tone)
                next_interval_modifier = 0
            else:
                # where there is a mismatch we have to step through the accidental intervals
                # and find which accidental of the next natural note our required step takes us to
                for (name, interval) in constants.accidental_intervals.items():
                    if cent_step == natural_step + interval:
                        scale_tones.append(natural_tone + name)
                        # we carry the cent difference between natural and accidental to the next iteration
                        next_interval_modifier = interval
                        break

        return scale_tones
