
music_theory_lib
====================

This python library provides functionality to find notes according to music theory

NOTE:
This is a personal project released as a helper for myself. It's written for the dual purpose of helping me to learn nusic theory and to experiment with coding tech. e.g. the python 3 conversion branch.

I'll endeavour to keep it working as I go on but it will be subject to change.


Release Notes
================

v0.0.1
--------

Starting classes representing scale and note. Ability to find scale and chord notes for a given root.