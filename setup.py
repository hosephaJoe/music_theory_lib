from setuptools import setup, find_packages

setup(
    version="0.0.dev1",
    author="Joe Lamont-Fisher",
    description="library for calculating music theory elements",
    name="music_theory_lib",
    packages=find_packages("src/python/"),
    package_dir={
        "": "src/python/"
        },
    python_requires='>=3.0'
    )
