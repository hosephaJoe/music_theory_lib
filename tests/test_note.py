
from music_theory_lib import base

import pytest


@pytest.mark.parametrize("input_string, expected", 
                         [("C", base.Note("C")), ("A#", base.Note("A", "#"))])
def test_note_from_string(input_string, expected):
    note = base.Note.from_str(input_string)
    assert note == expected


@pytest.mark.parametrize("input_value", [1, list()])
def test_note_from_invalid_input_type(input_value):
    with pytest.raises(TypeError):
        base.Note.from_str(input_value)


@pytest.mark.parametrize("note_string", ["Y", "G%", "GG", "G#b"])
def test_note_from_invalid_input_string(note_string):
    with pytest.raises(ValueError):
        base.Note.from_str(note_string)


def test_string_to_note_dict():
    note_dict = base.Note.string_to_note_dict("A#")

    assert note_dict == {'accidental': '#', 'natural': 'A'}
