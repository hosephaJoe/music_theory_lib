import pytest

from music_theory_lib import base

@pytest.mark.parametrize("input_string, expected", 
                         [("C", "C D E F G A B C"), ("A#", "A# B# C## D# E# F## G## A#")])
def test_get_major_scale_from_string(input_string, expected):

    s = base.Scale.major_scale_from_root_str(input_string)

    assert str(s) == expected
